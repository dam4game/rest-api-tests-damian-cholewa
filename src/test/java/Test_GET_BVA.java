import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_GET_BVA extends BaseTestClass {

    @DataProvider
    public Object[][] getValidPeselForBVA() {
        return new Object[][]{
                {"00810112366", "1800-01-01T00:00:00", "Female"},
                {"00810191314", "1800-01-01T00:00:00", "Male"},
                {"99923101503", "1899-12-31T00:00:00", "Female"},
                {"99923187916", "1899-12-31T00:00:00", "Male"},
                {"00010154366", "1900-01-01T00:00:00", "Female"},
                {"00010188291", "1900-01-01T00:00:00", "Male"},
                {"99123175863", "1999-12-31T00:00:00", "Female"},
                {"99123141273", "1999-12-31T00:00:00", "Male"},
                {"00210115521", "2000-01-01T00:00:00", "Female"},
                {"00210133693", "2000-01-01T00:00:00", "Male"},
                {"99323173928", "2099-12-31T00:00:00", "Female"},
                {"99323176679", "2099-12-31T00:00:00", "Male"},
                {"00410137365", "2100-01-01T00:00:00", "Female"},
                {"00410123474", "2100-01-01T00:00:00", "Male"},
                {"99523108904", "2199-12-31T00:00:00", "Female"},
                {"99523158817", "2199-12-31T00:00:00", "Male"},
                {"01610101301", "2201-01-01T00:00:00", "Female"},
                {"01610157418", "2201-01-01T00:00:00", "Male"},
                {"99723157003", "2299-12-31T00:00:00", "Female"},
                {"99723103912", "2299-12-31T00:00:00", "Male"},
        };
    }

    @Test(dataProvider = "getValidPeselForBVA")
    public void testGetValidPeselBVA(String validPesel, String expectedDateOFBirth, String expectedGender) {

        Response actualResponse = get("/Pesel?pesel=" + validPesel);
        Assert.assertEquals(actualResponse.statusCode(), 200);
        boolean actualIsValid = actualResponse.jsonPath().get("isValid");
        String actualDateOfBirth = actualResponse.jsonPath().getString("dateOfBirth");
        String actualGender = actualResponse.jsonPath().getString("gender");
        String actualErrorMessage = actualResponse.jsonPath().getString("errors");
        Assert.assertTrue(actualIsValid);
        Assert.assertEquals(actualDateOfBirth, expectedDateOFBirth);
        Assert.assertEquals(actualGender, expectedGender);
        Assert.assertEquals(actualErrorMessage, "[]");
    }
}
