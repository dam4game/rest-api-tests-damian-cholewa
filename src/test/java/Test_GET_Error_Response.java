import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_GET_Error_Response extends BaseTestClass {

    @DataProvider
    public Object[][] getInvalidPeselWithError() {
        return new Object[][]{
                {"00523", "[INVL]", "[Invalid length. Pesel should have exactly 11 digits.]"},
                {"0052313618a", "[NBRQ]", "[Invalid characters. Pesel should be a number.]"},
                {"00993136180", "[INVY, INVM, INVD]", "[Invalid year., Invalid month., Invalid day.]"},
                {"00523336185", "[INVD]", "[Invalid day.]"},
                {"00523136182", "[INVC]", "[Check sum is invalid. Check last digit.]"},
        };
    }

    @Test(dataProvider = "getInvalidPeselWithError")
    public void testInvalidPesel(String invalidPesel, String expectedErrorCode, String expectedErrorMessage) {

        Response actualResponse = get("/Pesel?pesel=" + invalidPesel);
        Assert.assertEquals(actualResponse.statusCode(), 200);
        boolean actualIsValid = actualResponse.jsonPath().get("isValid");
        String actualErrorCode = actualResponse.jsonPath().getString("errors.errorCode");
        String actualErrorMessage = actualResponse.jsonPath().getString("errors.errorMessage");
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualErrorCode, expectedErrorCode);
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
    }
}