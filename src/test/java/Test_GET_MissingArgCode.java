import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class Test_GET_MissingArgCode extends BaseTestClass {

    @Test
    public void testGetRequest_ResponseStatusCodeMissingPeselArgument() {
        int expectedStatusCode = 400;

        given()
                .when()
                .get("/Pesel?pesel=")
                .then()
                .assertThat()
                .statusCode(expectedStatusCode);
    }
}
