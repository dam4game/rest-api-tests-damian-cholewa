import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_GET_Leap_Years extends BaseTestClass {

        @DataProvider
        public Object[][] getValidPeselForLeapYear() {
            return new Object[][]{
                    {"04822919908", "1804-02-29T00:00:00", "Female"},
                    {"04822982612", "1804-02-29T00:00:00", "Male"},
                    {"72022999709", "1972-02-29T00:00:00", "Female"},
                    {"72022971910", "1972-02-29T00:00:00", "Male"},
                    {"16222959004", "2016-02-29T00:00:00", "Female"},
                    {"16222956216", "2016-02-29T00:00:00", "Male"},
                    {"60422965804", "2160-02-29T00:00:00", "Female"},
                    {"60422984113", "2160-02-29T00:00:00", "Male"},
            };
        }

        @DataProvider
        public Object[][] getInvalidPeselWithError() {
            return new Object[][]{
                    {"04823019904", "[INVD]"},
                    {"04823057218", "[INVD]"},
                    {"72023099705", "[INVD]"},
                    {"72023071916", "[INVD]"},
                    {"16223059000", "[INVD]"},
                    {"16223056212", "[INVD]"},
                    {"16223059000", "[INVD]"},
                    {"16223056212", "[INVD]"},
                    {"60423065800", "[INVD]"},
                    {"60423084119", "[INVD]"},
            };
        }

        @Test(dataProvider = "getValidPeselForLeapYear")
        public void testGetValidPeselLeapYear(String validPesel, String expectedDateOFBirth, String expectedGender) {

            Response actualResponse = get("/Pesel?pesel=" + validPesel);
            Assert.assertEquals(actualResponse.statusCode(), 200);
            boolean actualIsValid = actualResponse.jsonPath().get("isValid");
            String actualDateOfBirth = actualResponse.jsonPath().getString("dateOfBirth");
            String actualGender = actualResponse.jsonPath().getString("gender");
            String actualErrorMessage = actualResponse.jsonPath().getString("errors");
            Assert.assertTrue(actualIsValid);
            Assert.assertEquals(actualDateOfBirth, expectedDateOFBirth);
            Assert.assertEquals(actualGender, expectedGender);
            Assert.assertEquals(actualErrorMessage, "[]");
        }

        @Test(dataProvider = "getInvalidPeselWithError")
        public void testInvalidPeselLeapYear(String invalidPesel, String expectedErrorCode) {

            Response actualResponse = get("/Pesel?pesel=" + invalidPesel);
            Assert.assertEquals(actualResponse.statusCode(), 200);
            boolean actualIsValid = actualResponse.jsonPath().get("isValid");
            String actualErrorCode = actualResponse.jsonPath().getString("errors.errorCode");
            Assert.assertFalse(actualIsValid);
            Assert.assertEquals(actualErrorCode, expectedErrorCode);
        }
    }



