import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_GET_EQ extends BaseTestClass {

    @DataProvider
    public Object[][] getValidPeselForEQ() {
        return new Object[][]{
                {"23851582503", "1823-05-15T00:00:00", "Female"},
                {"66890462014", "1866-09-04T00:00:00", "Male"},
                {"03112223501", "1903-11-22T00:00:00", "Female"},
                {"44062909918", "1944-06-29T00:00:00", "Male"},
                {"80261233405", "2080-06-12T00:00:00", "Female"},
                {"12221255417", "2012-02-12T00:00:00", "Male"},
                {"77440180504", "2177-04-01T00:00:00", "Female"},
                {"23472243418", "2123-07-22T00:00:00", "Male"},
                {"50702823108", "2250-10-28T00:00:00", "Female"},
                {"81631348413", "2281-03-13T00:00:00", "Male"},
        };
    }

    @Test(dataProvider = "getValidPeselForEQ")
    public void testGetValidPeselEQ(String validPesel, String expectedDateOFBirth, String expectedGender) {

        Response actualResponse = get("/Pesel?pesel=" + validPesel);
        Assert.assertEquals(actualResponse.statusCode(), 200);
        boolean actualIsValid = actualResponse.jsonPath().get("isValid");
        String actualDateOfBirth = actualResponse.jsonPath().getString("dateOfBirth");
        String actualGender = actualResponse.jsonPath().getString("gender");
        String actualErrorMessage = actualResponse.jsonPath().getString("errors");
        Assert.assertTrue(actualIsValid);
        Assert.assertEquals(actualDateOfBirth, expectedDateOFBirth);
        Assert.assertEquals(actualGender, expectedGender);
        Assert.assertEquals(actualErrorMessage, "[]");
    }
}
