import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_GET_NBP_API_Error_Response {

    @DataProvider
    public Object[][] getInvalidRequest() {
        return new Object[][]{
                {"rates/a/eue/", "404 NotFound - Not Found - Brak danych"},
                {"tables/a/2020-01-01/2012-01-31/", "400 BadRequest - Błędny zakres dat / Invalid date range"},
                {"tables/a/2010-01-01/2012-01-31/", "400 BadRequest - Przekroczony limit 93 dni / Limit of 93 days has been exceeded"},
        };
    }

    @Test(dataProvider = "getInvalidRequest")
    public void testInvalidPesel(String invalidCurrency, String expectedErrorCode) {

        Response actualResponse = get("http://api.nbp.pl/api/exchangerates/" + invalidCurrency);
        String actualErrorCod = actualResponse.print();
        Assert.assertEquals(actualErrorCod, expectedErrorCode);
    }
}
